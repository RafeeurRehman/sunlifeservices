package com.sunfile.apis.SunfifeMicroServices.cantroller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sunfile.apis.SunfifeMicroServices.entities.Customer;
import com.sunfile.apis.SunfifeMicroServices.services.CustomerServices;

@RestController
@RequestMapping("/sun/api/customer")
public class CustomerCantroler {

	@Autowired
	private CustomerServices customerServices;


	  @GetMapping("/list")
	  public ResponseEntity<?> fetchAllAdmin(){
		
		  ResponseEntity<?> response = null;
			try {
				List<Customer> cList = (List<Customer>) customerServices.getAllCustomers();
				response = new ResponseEntity<List<Customer>>(cList,HttpStatus.OK);

			} catch (Exception e) {
				response = new ResponseEntity<String>(e.getMessage(), HttpStatus.FORBIDDEN);
			}
			return response;
	 }
	  
	 @PostMapping("/save")
	 public ResponseEntity<?> Save(@Valid @RequestBody Customer customer){
		ResponseEntity<?> response = null;
		try {
			Customer clinte = customerServices.createCustomer(customer);
			System.out.println("Customer Name is Lising  "+clinte.getFname()+" "+clinte.getLname());
			return response = new ResponseEntity<String>(clinte+"\n  Hi Exception \n"+"mY dATA Saving Admin Data Content Missing ", HttpStatus.FORBIDDEN);
		
		}catch(Exception ex) {
			ex.printStackTrace();
		}
			return response = new ResponseEntity<String>(customer+"\n  Hi Exception \n"+"mY dATA Saving Admin Data Content Missing ", HttpStatus.FORBIDDEN);
	  }


	  @PostMapping("/update/{id}")
	  public ResponseEntity<?> UpdateClient(@PathVariable long cid,@Valid @RequestBody Customer customer){
		  	customer = customerServices.updateCustomer(cid, customer);
			ResponseEntity<?> response = null;
			return response = new ResponseEntity<String>("Hi Exception \n"+" Updating Admin Content nmissing ", HttpStatus.FORBIDDEN);
	  }
	  
	  @DeleteMapping("/update/{id}")
	  public ResponseEntity<?>  deleteClient(@PathVariable long cid){
		  	customerServices.deleteCustomer(cid);
			ResponseEntity<?> response = new ResponseEntity<String>("Hi Exception \n"+" Updating Admin Content nmissing ", HttpStatus.FORBIDDEN);
			return response;
		}


	  
		/*	@PostMapping("/create")
	public ResponseEntity<?> dataPaginationlists(@Valid @RequestBody Customer customer){
		customer = customerServices.createCustomer(customer);
		ResponseEntity<?> response = new ResponseEntity(customer,HttpStatus.OK); 
		return response = new ResponseEntity<String>("Hi Exception \n"+"mY dATA Pagination ", HttpStatus.FORBIDDEN);
	}
	

	 * @RequestMapping(value="list", method=RequestMethod.GET,
	 * produces="application/json") public ResponseEntity<?> findAll() {
	 * ResponseEntity<?> response = null; try { List<Customer> cidList =
	 * (List<Customer>) customerServices.findAll();
	 * 
	 * System.out.println("Rafee"+" Ur "+"Rehman"+" Soomro"+cidList);
	 * 
	 * response = new ResponseEntity<List<Customer>>(cidList,HttpStatus.OK); } catch
	 * (Exception e) { } return response; }
	 */
		


	  	  
	  @GetMapping("/countries")
	  public ResponseEntity getAllCountries() {
	    List<List<String>> Countrieslist = new ArrayList<>();
	      List<String> listEn = new ArrayList<>();
	      listEn.add("Jordan");
	      listEn.add("UAE");
	      listEn.add("Bahrain");
	      listEn.add("Kuwait");
	      listEn.add("Oman");
	      listEn.add("Qatar");
	      listEn.add("Egypt");
	      listEn.add("All Gulf Countries");
	      listEn.add("Others");
	      Countrieslist.add(listEn);
		ResponseEntity<?> response = null;
		return response = new ResponseEntity<String>("Hi Exception \n"+listEn, HttpStatus.FORBIDDEN);
			
	      //return new BasicResponse<>(true, null, null, HttpStatus.OK, Countrieslist);

	  }
	  
	  
	  
	  
	  
	  
	  }
			
	
	
	/* 
	 * public ResponseEntity<?> remove(@RequestParam("studentID") Long studentID) {
	 * ResponseEntity<?> response = null; try { boolean result =
	 * studentsService.remove(studentID); if(result) { response = new
	 * ResponseEntity<String>("Deleted Successfully", HttpStatus.OK); } else {
	 * response = new ResponseEntity<String>("Student Not Found",
	 * HttpStatus.FORBIDDEN); } } catch (Exception e) { e.printStackTrace();
	 * response = new ResponseEntity<String>(e.getMessage(), HttpStatus.FORBIDDEN);
	 * } return response; }
	 */
