package com.sunfile.apis.SunfifeMicroServices.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sunfile.apis.SunfifeMicroServices.entities.Customer;

@Repository
public interface CustomerReposetory extends JpaRepository<Customer, Long>{

}
