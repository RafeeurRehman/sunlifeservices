package com.sunfile.apis.SunfifeMicroServices.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sunfile.apis.SunfifeMicroServices.entities.Customer;
import com.sunfile.apis.SunfifeMicroServices.enums.response.RecodeNotFoundException;
import com.sunfile.apis.SunfifeMicroServices.repository.CustomerReposetory;

@Service
@Transactional
public class CustomerServices implements ProductService{

	@Autowired
	private CustomerReposetory customerRepository;
	
	@Override
	public Customer createCustomer(Customer customer) {
		return customerRepository.save(customer);
	}


	@Override
	public java.util.List<Customer> getAllCustomers() {
		return this.customerRepository.findAll();
	}

	@Override
	public Customer getCustomerById(long id) {		
		Optional<Customer> customerdb = this.customerRepository.findById(id);
		if(customerdb.isPresent()) {
			return customerdb.get();
		}else {
			throw new RecodeNotFoundException("Recod not Found "+customerdb.get());
		} 		
	}	

	@Override
	public void deleteCustomer(long id) {
		Optional<Customer> customerdb = this.customerRepository.findById(id);
		if(customerdb.isPresent()) {
			this.customerRepository.delete(customerdb.get());
		}else {
			throw new RecodeNotFoundException("Recod not Found "+customerdb.get());
		} 		
	}
	
	@Override
	public Customer updateCustomer(long id,Customer customer) {
		Optional<Customer> customerdb = this.customerRepository.findById(id);
		if(customerdb.isPresent()) {
			Customer updateCustomer = customerdb.get();
			
	        updateCustomer.setPolicyNo(customer.getPolicyNo()); 
	        updateCustomer.setPolicyPriority(customer.getPolicyPriority()); 
	        updateCustomer.setFname(customer.getFname()); 
	        updateCustomer.setLname(customer.getLname()); 
	        updateCustomer.setTravelerType(customer.getTravelerType()); 
	        updateCustomer.setProductType(customer.getProductType()); 
	        updateCustomer.setCountryOfOrigan(customer.getCountryOfOrigan()); 
	        updateCustomer.setProvince(customer.getProvince()); 
	        updateCustomer.setZipCode(customer.getZipCode()); 
	        updateCustomer.setIsCanadian(customer.getIsCanadian()); 
	        updateCustomer.setPhoneNumberPrmary(customer.getPhoneNumberPrmary()); 
	        updateCustomer.setPhoneNumberSecondry(customer.getPhoneNumberSecondry()); 
	        updateCustomer.setPhoneNumbrOffice(customer.getPhoneNumbrOffice()); 
	        updateCustomer.setEmail(customer.getEmail()); 
	        updateCustomer.setIsActive(customer.getIsActive()); 
	        updateCustomer.setOffice(customer.getOffice()); 
			updateCustomer.setAddress1(customer.getAddress1());
	        updateCustomer.setAddress2(customer.getAddress2()); 
			updateCustomer.setComponyName(customer.getComponyName());
			updateCustomer.setComponyNumber(customer.getComponyNumber());
			updateCustomer.setDateOfBirth(customer.getDateOfBirth());
			updateCustomer.setDateOfUsingService(customer.getDateOfUsingService());
			updateCustomer.setApplicationDate(customer.getApplicationDate());
			updateCustomer.setEffectiuveDate(customer.getEffectiuveDate());
			updateCustomer.setExpiryDate(customer.getExpiryDate());
			updateCustomer.setTripLength(customer.getTripLength());
			updateCustomer.setPolicyholder(customer.getPolicyholder());
			updateCustomer.setFallowUpCallReminder(customer.getFallowUpCallReminder());
			updateCustomer.setClinteCallResponseList(customer.getClinteCallResponseList());
			updateCustomer.setLastDateOfSubmition(customer.getLastDateOfSubmition());
			updateCustomer.setPaymentStatus(customer.getPaymentStatus());
			updateCustomer.setAmountPaid(customer.getAmountPaid());
			updateCustomer.setAmountRemaining(customer.getAmountRemaining());
			updateCustomer.setTotalCommissionAmount(customer.getTotalCommissionAmount());
			updateCustomer.setComissionStatus(customer.getComissionStatus());
			updateCustomer.setCommissionPersentage(customer.getCommissionPersentage());
			updateCustomer.setDocuments(customer.getDocuments());
			updateCustomer.setDatapdffiles(customer.getDatapdffiles());
			return updateCustomer;	
		}else {
			new RecodeNotFoundException("Recod not found with id "+id);
		}
		// TODO Auto-generated method stub
		return null;
	}

	

	
	//		customer = customerRepository.save(customer); 
	//		customerList = (List) customerRepository.findAll();		
			

/*	
	//This is used to save or update a studentRecord
	public Customer save(Customer students){
		try { students = customerRepository.save(students); } catch (Exception e) {	e.printStackTrace(); }
		return students;
	}



	
	public List<Customer> findAll() {
		List<Customer> stdList = null;
		try { stdList = (List<Customer>) customerRepository.findAll(); } catch (Exception e) { e.printStackTrace(); }
		return stdList;
	}
	
	public Customer findAll(Long cid){
		Customer students = null;
		try { students = customerRepository.findOne(cid); } catch (Exception e) { e.printStackTrace(); }
		return students;
	}
	
	public boolean remove(Long stdID) {
		boolean result = false;
		try { Customer students = customerRepository.findOne(stdID);
			if(students != null) { customerRepository.delete(stdID);
				result = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
*/
	
}