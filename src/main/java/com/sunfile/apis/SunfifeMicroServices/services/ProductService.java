package com.sunfile.apis.SunfifeMicroServices.services;

import java.util.List;

import com.sunfile.apis.SunfifeMicroServices.entities.Customer;

public interface ProductService {
	Customer createCustomer(Customer customer);
	Customer updateCustomer(long cid,Customer customer);
	List<Customer> getAllCustomers(); 
	Customer getCustomerById(long id);
	void deleteCustomer(long id);
}
