package com.sunfile.apis.SunfifeMicroServices.enums.response;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus
public class RecodeNotFoundException extends RuntimeException {
	public RecodeNotFoundException(String message){
		super(message);
	}
	public RecodeNotFoundException(String message,Throwable throwable){
		super(message,throwable);
	}
}
