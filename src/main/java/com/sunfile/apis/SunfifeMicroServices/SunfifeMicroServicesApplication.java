package com.sunfile.apis.SunfifeMicroServices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SunfifeMicroServicesApplication {
	public static void main(String[] args) {
		SpringApplication.run(SunfifeMicroServicesApplication.class, args);
	}
}
